package com.fitbasewebchecklist.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {

    static WebDriver driver;
	
	public SignUpPage(WebDriver driver){
		PageFactory.initElements(driver, this);
	}
	
	//####################################################
	//SignUp page page objects
	//####################################################
	
	@FindBy(xpath="//*[@id='tabsignup']")
	public WebElement tab_SignUpInLoginForm;
	 
	@FindBy(how = How.XPATH, using ="//*[@id='firstName']") 
	public WebElement txt_FirstName;
	
	@FindBy(how = How.XPATH, using ="//*[@id='lastName']") 
	public WebElement txt_LastName;
	
	@FindBy(how = How.XPATH, using="//*[@id='email']")
	public WebElement txt_EmalAddress;
	
	@FindBy(how = How.XPATH, using="//*[@id='signuppassword']")
	public WebElement txt_Password;
	
	@FindBy(how=How.XPATH, using = "//*[@id=\"profile-create-form\"]/div[5]/div/label/span[1]")
	public WebElement checkbox_Terms;
	
	@FindBy(how=How.XPATH, using = "//*[@id='submitbtn']")
	public WebElement btn_SignUp;
	
	@FindBy(how=How.XPATH, using="//*[@id='email-error']")
	public WebElement err_EmailAddAlreadyExists;
	
	//###########################################################
	//Health details setUp page page objects
	//###########################################################
	
	@FindBy(how=How.XPATH, using="//button[@type='submit' and @ng-click='skipHealthprofile()']")
	public WebElement btn_Skip;
	
	@FindBy(xpath="//a[text()='Not now']")
	public WebElement btn_NotNow;
		
	//####### ####### ######
	// Add Your Health Profile page objects
	//####### ####### ######	
    @FindBy(how=How.XPATH, using="//h1[text()= 'Tell Us About Yourself']")
    public WebElement txt_healthDetails;
	
	@FindBy(how = How.XPATH, using="//a[@ng-click='skip()']")
	public WebElement btn_NotNowGoToDashboard;
	
	@FindBy(how=How.XPATH, using="//a[@ng-click='addHealth()']")
	public WebElement btn_OkToFillHealthDetails;
	
	@FindBy(how=How.XPATH, using="//input[@id='heightfeet' and @name='heightfeet']")
	public WebElement btn_Height_Feet;
	
	@FindBy(how=How.XPATH, using="//input[@id='heightfeet' and @name='heightinch']")
	public WebElement btn_Height_Inches;
	
	@FindBy(how=How.XPATH, using="//input[@id='currentWeight']")
	public WebElement btn_Weight_LBS;
	
	@FindBy(how=How.XPATH, using="//select[@ng-model='months']")
	public WebElement drp_Months;
	
	@FindBy(how=How.XPATH, using="//select[@ng-model='date']")
	public WebElement drp_Date;
	
	@FindBy(how=How.XPATH, using="//select[@ng-model='year']")
	public WebElement drp_Year;
	
	@FindBy(how=How.XPATH, using="//select[@id='gender']")
	public WebElement drp_Gender;
	
	@FindBy(how=How.XPATH, using="//select[@name='useractivitylevel']")
	public WebElement drp_ActivityLevel;
	
	@FindBy(how=How.XPATH, using="//button[@class='btn btn-lg btn-default ng-binding']")
	public WebElement btn_Next;
	
	//########### WORKOUT ##############
	@FindBy(xpath="//button[@class='btn btn-lg btn-default ng-binding']")
	public WebElement btn_WorkoutActivity_Next;
	
	//########### GOAL #################
	@FindBy(xpath="//a[@class='btn btn-lg btn-info ng-binding']")
	public WebElement btn_GoalTab_Later;
	
}
