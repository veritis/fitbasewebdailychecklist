package com.fitbasewebchecklist.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class DevicesPage{
	 static WebDriver driver;

public class FitbitDevicePage {
	
	public FitbitDevicePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH, using="//a[text()='Devices']")
	public WebElement lnk_Devices;
	
	@FindBy(xpath="//button[@ng-click=\"connect('FITBIT')\"]")
	public WebElement btn_Connect_Fitbit;
	
	@FindBy(how=How.XPATH, using="//input[@placeholder=\"Your email address\"]")
	public WebElement txt_Fitbit_UserName;
	
	@FindBy(how=How.XPATH, using="//input[@placeholder=\"Enter your password\"]")
	public WebElement txt_Fitbit_Passowrd;
	
	@FindBy(how=How.XPATH, using="//*[@id=\'ember694\']")
	public WebElement btn_Fitbit_Submit;
	
	@FindBy(xpath="")
	public WebElement confirm_AuthenticationPage;
	
	@FindBy(xpath="//button[@ng-show='syncFitbit']")
	public WebElement btn_Fitbit_Sync;
	
	@FindBy(xpath="//span[@ng-show=\"showFitbitLastSyncTime\"]")
	public WebElement msg_LastSync;
	
	@FindBy(how=How.XPATH, using="//a[@ng-show=\"disconnectFitbit\"]")
	public WebElement btn_Fitbit_Disconnect;
	
	@FindBy(how=How.XPATH, using="//button[text()=\"YES, I'M SURE\"]")
	public WebElement btn_Fitbit_Confirm_Disconnect;
	
}
public class MisfitDevicePage{

    public MisfitDevicePage(WebDriver driver) {
	PageFactory.initElements(driver, this);
   }
    
    @FindBy(how=How.XPATH, using="//a[text()='Devices']")
	public WebElement lnk_Devices;
    
    @FindBy(how=How.XPATH, using="//button[@ng-hide=\"activateMisfit\"]")
	public WebElement btn_Connect_Misfit;
    
    @FindBy(how=How.XPATH, using="//input[@name=\"email\"]")
    public WebElement txt_Misfit_UserName;
    
    @FindBy(how=How.XPATH, using="//input[@name=\"password\"]")
    public WebElement txt_Misfit_Passowrd;
    
    @FindBy(how=How.XPATH, using="//*[@id=\"login-btn\"]/span")
    public WebElement btn_Misfit_Submit;
    
    @FindBy(how=How.XPATH, using="//button[@ng-show=\"syncMisfit\"]")
    public WebElement btn_Misfit_Sync;
    
	@FindBy(xpath="//span[@ng-show=\"showMistfitLastSyncTime\"]")
	public WebElement msg_LastSync;
    
    @FindBy(how=How.XPATH, using="//a[@ng-show=\"disconnectMisfit\"]")
    public WebElement btn_Misfit_Disconnect;
    
    @FindBy(how=How.XPATH, using="//button[text()=\"YES, I'M SURE\"]")
    public WebElement btn_Misfit_Confirm_Disconnect;
    
}

public class WithingsDevicesPage{
	public WithingsDevicesPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.XPATH, using="//a[text()='Devices']")
	public WebElement lnk_Devices;
	
	@FindBy(how=How.XPATH, using="//button[@ng-hide=\"activateWithings\"]")
	public WebElement btn_Connect_Withings;
	
	@FindBy(xpath="//input[@placeholder=\"Email address\"]")
	public WebElement txt_Withings_UserName;
	
	@FindBy(xpath="//input[@placeholder=\"Password\"]")
	public WebElement txt_Withings_Passowrd;
	
	@FindBy(xpath="//button[text()=\"Sign in\"]")
	public WebElement btn_Withings_Submit;
	
	@FindBy(how=How.XPATH, using="//button[text()=\"Allow this app\"]")
	public WebElement btn_AuthenticationPage_Withings;
	
	@FindBy(xpath="//button[@ng-show=\"syncWithings\"]")
	public WebElement btn_Withings_Sync;
	
	@FindBy(xpath="//span[@ng-show=\"showWithingsSyncTime\"]")
	public WebElement msg_LastSync;
	
	@FindBy(xpath="//a[@ng-show=\"disconnectWithings\"]")
	public WebElement btn_Withings_Disconnect;
	
	@FindBy(xpath="//button[text()=\"YES, I'M SURE\"]")
	public WebElement btn_Withings_Confirm_Disconnect;
}

public class StravaPage{
	public StravaPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//a[text()='Devices']")
	public WebElement lnk_Devices;
	
	@FindBy(xpath="//button[@ng-hide=\"activateStrava\"]")
	public WebElement btn_Connect_Strava;
	
	@FindBy(xpath="//*[@id=\'email\']")
	public WebElement txt_Strava_UserName;
	
	@FindBy(xpath="//*[@id=\'password\']")
	public WebElement txt_Strava_Passowrd;
	
	@FindBy(xpath="//*[@id=\'login-button\']")
	public WebElement btn_Strava_Submit;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"authorize\"]")
	public WebElement btn_AuthenticationPage_Strava;
	
	@FindBy(xpath="//button[@ng-show=\"syncStrava\"]")
	public WebElement btn_Strava_Sync;
	
	@FindBy(xpath="//span[@ng-show=\"showStravaLastSyncTime\"]")
	public WebElement msg_LastSync;
	
	@FindBy(xpath="//a[@ng-show=\"disconnectStrava\"]")
	public WebElement btn_Strava_Disconnect;
	
	@FindBy(xpath="//button[text()=\"YES, I'M SURE\"]")
	public WebElement btn_Strava_Confirm_Disconnect;
}

public class GoogleFitPage{
	public GoogleFitPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//a[text()='Devices']")
	public WebElement lnk_Devices;
	
	@FindBy(xpath="//button[@ng-hide=\"activateGoogleFit\"]")
	public WebElement btn_Connect_GoogleFit;
	
	@FindBy(xpath="//input[@type='email']")
	public WebElement txt_Gmail_Email;
	
	@FindBy(xpath="//div[@id='identifierNext']")
	public WebElement btn_GmailEmail_Next;
	
	@FindBy(xpath="//input[@type='password']")
	public WebElement txt_Gmail_Password;
	
	@FindBy(xpath="//*[@id=\'passwordNext\']")
	public WebElement btn_GmailPassword_Next;
	
	@FindBy(xpath="//*[@id=\'submit_approve_access\']/div/button/div[2]")
	public WebElement btn_FitbaseGmailAllow;
	
	
	@FindBy(xpath="//*[@id=\"submit_approve_access\"]/span/span")
	public WebElement btn_AuthenticationPage_GoogleFit;
	
	@FindBy(xpath="//button[@ng-show=\"syncGoogleFit\"]")
	public WebElement btn_GoogleFit_Sync;
	
	@FindBy(xpath="//span[@ng-show=\"showGoogleFitLastSyncTime\"]")
	public WebElement msg_GoogleFit_LastSync;
	
	@FindBy(xpath="//a[@ng-show=\"disconnectGoogleFit\"]")
	public WebElement btn_GoogleFit_Disconnect;
	
	@FindBy(xpath="//button[text()=\"YES, I'M SURE\"]")
	public WebElement btn_GoogleFit_Confirm_Disconnect;

}
}
