package com.fitbasewebchecklist.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LogoutPage {

	static WebDriver driver;
	
	public LogoutPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	// Menu page object
	@FindBy(xpath="//ul[@class='nav navbar-top-links navbar-right userdpw pzero ng-scope']/li/a[@data-toggle='dropdown']")
	public WebElement drp_Menu;
	
	// Logout button
	@FindBy(how=How.XPATH, using="//a[contains(text(),'Logout')]")
	public WebElement lnk_Logout;
	
	
	
}
