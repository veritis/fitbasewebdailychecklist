package com.fitbasewebchecklist.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage{
    
	static WebDriver driver;

    public LoginPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	}
	
    //##### Normal login user ##########
    ////////////////////////////////////
	@FindBy(how = How.XPATH, using ="//*[@id='signuplogin']/a")
	@CacheLookup
	public WebElement btn_LoginAndSignUp;
	
	@FindBy(xpath="//*[@id=\'username\']")
	public WebElement txt_UserName;
	
	@FindBy(xpath="//*[@id=\'password\']")
	public WebElement txt_Password;
		
	@FindBy(xpath="//*[@id=\'loginemail\']")
	public WebElement btn_Submit;
	
	
	//######### Google login user ##########
	////////////////////////////////////////
	@FindBy(xpath="//*[@id=\'sign-in-google\']")
	public WebElement tab_GoogleLogin;
	
	@FindBy(xpath="//*[@id=\'identifierId\']")
	public WebElement txt_Gmail_Email;
	
	@FindBy(xpath="//div[@id='identifierNext']")
	public WebElement btn_GmailEmail_Next;
	
	@FindBy(xpath="//*[@id=\'password\']/div[1]/div/div[1]/input")
	public WebElement txt_Gmail_Password;
	
	@FindBy(xpath="//*[@id=\'passwordNext\']")
	public WebElement btn_GmailPassword_Next;
	
	@FindBy(xpath="//*[@id=\'submit_approve_access\']/div/button/div[2]")
	public WebElement GmailAllowButton;
	
	
	
	//########### Facebook user ##############
	/////////////////////////////////////////
	@FindBy(how=How.XPATH, using="//*[@id=\'sign-in-fb\']")
	public WebElement tab_FacebookLogin;
	
	@FindBy(how=How.XPATH, using="//*[@id=\'email\']")
	public WebElement txt_Facebook_UserName;
	
	@FindBy(how=How.XPATH, using="//*[@id=\'pass\']")
	public WebElement txt_Facebook_Password;
	
	@FindBy(how=How.XPATH, using="//*[@id=\'u_0_0\']")
	public WebElement btn_FacebookLogin;
	
	
	//########### Forgot password ###########
	/////////////////////////////////////////
	@FindBy(how=How.XPATH, using="//*[@id=\'user-login-form\']/div[4]/span/a")
	public WebElement lnk_fotgotPassword;
	
	@FindBy(how=How.XPATH, using="//*[@id=\'email\']")
	public WebElement txt_forgotPassword;
	
	@FindBy(how=How.XPATH, using="//*[@id=\'profile-create-form\']/button")
	public WebElement btn_forgotPassword;

	@FindBy(how=How.XPATH, using="/html/body/div[1]/div/div/div/form/h4")
	public WebElement msg_forgotPasswordSuccessMessage;

    }
