package com.fitbasewebchecklist.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LiveConnectPage {

	static WebDriver driver;
	public LiveConnectPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//p[text()='No live sessions available']")
	public WebElement msg_NoLiveSessionsAvailable;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"livenotification-content\"]/a/em")
	public WebElement btn_LiveSessionsNotificationsPopup;
	
	@FindBy(xpath="//button[@class='ng-binding accept']")
	public WebElement btn_LiveSession;
	
	@FindBy(xpath="//*[@id=\'icons\']")
	public WebElement LiveVideoSessionControlButtons;
	
}


