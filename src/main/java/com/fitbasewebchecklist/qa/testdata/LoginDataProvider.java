package com.fitbasewebchecklist.qa.testdata;

import org.testng.annotations.DataProvider;

public class LoginDataProvider {
	
	@DataProvider(name="FitbitLogin")// Existing user login
	public static Object[][] getDataFromFitbitLogInDataProvider(){
	return new Object[][] {{"sunilkumar12@mailinator.com", "password"}};
	}
	
	@DataProvider(name="GoogleFitLogin")// Existing user login
	public static Object[][] getDataFromGoogleFitLogInDataProvider(){
	return new Object[][] {{"sunilkumar16@mailinator.com", "password"}};
	}
	
	@DataProvider(name="MisfitLogin")// Existing user login
	public static Object[][] getDataFromMisfitLogInDataProvider(){
	return new Object[][] {{"sunilkumar10@mailinator.com", "password"}};
	}
	
	@DataProvider(name="StravaLogin")// Existing user login
	public static Object[][] getDataFromStravaLogInDataProvider(){
	return new Object[][] {{"sunilkumar11@mailinator.com", "password"}};
	}
	
	@DataProvider(name="WithingsLogin")// Existing user login
	public static Object[][] getDataFromWithingsLogInDataProvider(){
	return new Object[][] {{"sunilkumar13@mailinator.com", "password"}};
	}
	
	@DataProvider(name="UserLogin")// Existing user login
	public static Object[][] getDataFromUserLogInDataProvider(){
	return new Object[][] {{"u1@mailinator.com", "password"}};
	}
	
	@DataProvider(name="UserProdLogin")// Existing user login
	public static Object[][] getDataFromUserProdLogInDataProvider(){
	return new Object[][] {{"stevejhonson@mailinator.com", "password"}};
	}
	
	@DataProvider(name="SessionbookUserLogin")// Existing user login
	public static Object[][] getDataFromUserLogInsDataProvider(){
	return new Object[][] {{"sunilkumar10@mailinator.com", "password"}};
	}
	
	@DataProvider(name="SessionbookpaypalUserLogin")// Existing user login
	public static Object[][] getDataFromUserLoginpaypalsDataProvider(){
	return new Object[][] {{"sunilkumar16@mailinator.com", "password"}};
	}
	
	@DataProvider(name="UserLogin1")// Existing user login
	public static Object[][] getDataFromTrainerLogInDataProvider(){
	return new Object[][] {{"trainer@mailinator.com", "password"}};
	}
	
	@DataProvider(name="UserSignup") // New user sign up
	public static Object[][] getDataFromUserSignUpDataProvider(){
	return new Object[][]{{"First Name", "Last Name", "password"}};
   }
	
	@DataProvider(name="GoogleLogin") // Google login
	public static Object[][] getDataFromGoogleLoginDataProvider(){
			return new Object[][]{{"fitbase.mail1@gmail.com", "testing123$"}};
		}			
	//	return new Object[][] {{"priyaindugula1", "priyalaya"}};
		
	@DataProvider(name="VideoStream") // Google login
	public static Object[][] getDataFromVideoDataProvider(){
		return new Object[][] {
			{"u1@mailinator.com" }};
	}
	@DataProvider(name="ForgotPassword") // Google login
	public static Object[][] getDataFromForgotPasswordDataProvider(){
		return new Object[][] {
			{"sunilkumar20@mailinator.com"}};
	}
	
	@DataProvider(name="FacebookLogin") // Facebook login
	public static Object[][] getDataFromDataProvider(){
		return new Object[][] {{"fitbase.mail@gmail.com", "testing123$"}};
		
	}
}
