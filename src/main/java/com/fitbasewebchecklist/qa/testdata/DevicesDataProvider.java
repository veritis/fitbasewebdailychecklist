package com.fitbasewebchecklist.qa.testdata;

import org.testng.annotations.DataProvider;

public class DevicesDataProvider {

	@DataProvider(name="FitbitDevice")
	public static Object[][] getDataFromFitbitDevice(){
		return new Object[][] {
			{"priyaindugula1@gmail.com"}, 
			{"password"}};	
	}
	
	@DataProvider(name="MisfitDevice")
	public static Object[][] getDataFromMisfitDevice(){
		return new Object[][] {{"vijaykumarkalpagur@gmail.com"}, {"Testing@123"}};
	}
	
	@DataProvider(name="GoogleFitDevice")
	public static Object[][] getDataFromGooglefitDevice(){
		return new Object[][] {{"sunilkumartestingfitbase@gmail.com"}, {"Testing@123"}};
	}
	
	@DataProvider(name="WithingsDevice")
	public static Object[][] getDataFromWithingsDevice(){
		return new Object[][] {
			{"chintu746@gmail.com"},
			{"9642464740"}
		};
	}
	
	@DataProvider(name="Strava")
	public static Object[][] getDataProviderStravaDevice(){
		return new Object[][] {{"vijaykumarkalpagur@gmail.com", "password"}};
	}
}
