package com.fitbasewebchecklist.qa.actions;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fitbasewebchecklist.qa.pages.DevicesPage;
import com.fitbasewebchecklist.qa.utilities.Log;


public class DevicesAction {
	
	public static WebDriver driver;
	
	public static void syncWithFitbitDevice(WebDriver driver) throws InterruptedException {
		
		DevicesPage.FitbitDevicePage fitbit=new DevicesPage().new FitbitDevicePage(driver);
		WebDriverWait wait=new WebDriverWait(driver, 150);
		
		wait.until(ExpectedConditions.elementToBeClickable(fitbit.lnk_Devices)).click();
		Log.info("Click action performed on Devices main link...");
		Thread.sleep(2500);
		
		if(fitbit.btn_Fitbit_Sync.isDisplayed()==false) {
		wait.until(ExpectedConditions.elementToBeClickable(fitbit.btn_Connect_Fitbit)).click();
		Log.info("Click action performed on Fitbit connect button...");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(fitbit.txt_Fitbit_UserName)).sendKeys("priyaindugula1@gmail.com");
		Log.info("Click action performed on fitbit username field and entered usernamame in it...");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(fitbit.txt_Fitbit_Passowrd)).sendKeys("password");
		Log.info("Click action performed on fitbit password field and entered password in it...");
		Thread.sleep(2000);
		wait.until(ExpectedConditions.elementToBeClickable(fitbit.btn_Fitbit_Submit)).click();
		Log.info("Click action performed on Fitbit Login button in fitbit login page...");
		Thread.sleep(15000);
		}
		else {
			Log.info("Fitbit device has already connected...");
		}
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(fitbit.btn_Fitbit_Sync)).click();
		Log.info("Click action performed on Fitbit Sync button...");
		Thread.sleep(3000);
		Assert.assertTrue(fitbit.msg_LastSync.isDisplayed());
		Log.info("Connecting & syncing with fitbit device has verified successfully....");
		Thread.sleep(3500);
	
	}
	
	public static void syncWithMisfitDevice(WebDriver driver) throws InterruptedException {
		DevicesPage.MisfitDevicePage misfit=new DevicesPage().new MisfitDevicePage(driver);
		WebDriverWait wait=new WebDriverWait(driver, 80);
		
		wait.until(ExpectedConditions.elementToBeClickable(misfit.lnk_Devices)).click();
		Log.info("Click action performed on main Devices link...");
		Thread.sleep(2500);

        if(misfit.btn_Misfit_Sync.isDisplayed()==false) {
        wait.until(ExpectedConditions.elementToBeClickable(misfit.btn_Connect_Misfit)).click();
        Log.info("Click action perfromed on Misfit link...");
		wait.until(ExpectedConditions.elementToBeClickable(misfit.txt_Misfit_UserName)).sendKeys("vijaykumarkalpagur@gmail.com");
		Log.info("Entered Misfit username...");
		wait.until(ExpectedConditions.elementToBeClickable(misfit.txt_Misfit_Passowrd)).sendKeys("Testing@123");
		Log.info("Entered misfit password...");
		
		wait.until(ExpectedConditions.elementToBeClickable(misfit.btn_Misfit_Submit)).click();
		Log.info("Click action performed on Misfit submit button....");
		Thread.sleep(6000);
        }else {
			Log.info("Misfit device has already connected...");
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(misfit.btn_Misfit_Sync)).click();
		Log.info("Click action performed on Misfit Sync button...");
		Thread.sleep(3000);
		Assert.assertTrue(misfit.msg_LastSync.isDisplayed());
		Log.info("Connecting & syncing with misfit device has verified successfully....");
		Thread.sleep(5500);
		
		}
	
	public static void syncWithWithings(WebDriver driver) throws InterruptedException {
		DevicesPage.WithingsDevicesPage withings=new DevicesPage(). new WithingsDevicesPage(driver);
		WebDriverWait wait=new WebDriverWait(driver,90);
		
		wait.until(ExpectedConditions.elementToBeClickable(withings.lnk_Devices)).click();
		Log.info("Click action performed on main Devices link...");
		Thread.sleep(2500);
		
		if(withings.btn_Withings_Sync.isDisplayed()==false) {
	        wait.until(ExpectedConditions.elementToBeClickable(withings.btn_Connect_Withings)).click();
	        Log.info("Click action perfromed on Withings link...");
			wait.until(ExpectedConditions.elementToBeClickable(withings.txt_Withings_UserName)).sendKeys("chintu746@gmail.com");
			Log.info("Entered Withings username...");
			wait.until(ExpectedConditions.elementToBeClickable(withings.txt_Withings_Passowrd)).sendKeys("9642464740");
			Log.info("Entered Withings password...");
			
			wait.until(ExpectedConditions.elementToBeClickable(withings.btn_Withings_Submit)).click();
			Log.info("Click action performed on Withings submit button....");
			
			//driver.findElement(By.xpath("//*[@id=\"user_selection\"]/div/div[1]/div[2]/div/div/a[1]")).click();
			Thread.sleep(5000);
			wait.until(ExpectedConditions.elementToBeClickable(withings.btn_AuthenticationPage_Withings)).click();
			Thread.sleep(7000);
	        }else {
				Log.info("Withings device has already connected...");
			}
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(withings.btn_Withings_Sync)).click();
			Log.info("Click action performed on Withings Sync button...");
			Thread.sleep(3000);
			Assert.assertTrue(withings.msg_LastSync.isDisplayed());
			Log.info("Connecting & syncing with Withings device has verified successfully....");
			Thread.sleep(5500);
			
			JavascriptExecutor js = (JavascriptExecutor) driver; 
			//This will scroll up the page  
			js.executeScript("window.scrollBy(0,-250)");
			Thread.sleep(2000);
	
	}
	
	public static void syncWithStrava(WebDriver driver) throws InterruptedException {
		DevicesPage.StravaPage strava=new DevicesPage(). new StravaPage(driver);
		WebDriverWait wait=new WebDriverWait(driver,90);
		
		wait.until(ExpectedConditions.elementToBeClickable(strava.lnk_Devices)).click();
		Log.info("Click action performed on main Devices link...");
		Thread.sleep(5000);
		
		if(strava.btn_Strava_Sync.isDisplayed()==false) {
			
			Assert.assertTrue(strava.btn_Connect_Strava.isDisplayed());
	        wait.until(ExpectedConditions.elementToBeClickable(strava.btn_Connect_Strava)).click();
	        Log.info("Click action perfromed on Strava link...");
	        Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(strava.txt_Strava_UserName)).sendKeys("vijaykumarkalpagur@gmail.com");
			Log.info("Entered Strava username...");
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(strava.txt_Strava_Passowrd)).sendKeys("password");
			Log.info("Entered Strava password...");
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(strava.btn_Strava_Submit)).click();
			Log.info("Click action performed on Strava submit button....");
			
			Thread.sleep(2000);
			wait.until(ExpectedConditions.elementToBeClickable(strava.btn_AuthenticationPage_Strava)).click();
			Thread.sleep(8000);
	        }else {
				Log.info("Strava device has already connected...");
			}
			
			wait.until(ExpectedConditions.elementToBeClickable(strava.btn_Strava_Sync)).click();
			Log.info("Click action performed on Strava Sync button...");
			Thread.sleep(3000);
			Assert.assertTrue(strava.msg_LastSync.isDisplayed());
			Log.info("Connecting & syncing with Strava device has verified successfully....");
			Thread.sleep(7000);
			
//			JavascriptExecutor js = (JavascriptExecutor) driver; 
//			//This will scroll up the page  
//			js.executeScript("window.scrollBy(0,-250)");
//			Thread.sleep(2000);
	
	}
	
	public static void syncWithGoogleFit(WebDriver driver) throws InterruptedException {
		DevicesPage.GoogleFitPage gfit=new DevicesPage(). new GoogleFitPage(driver);
		WebDriverWait wait=new WebDriverWait(driver,90);
		
		wait.until(ExpectedConditions.elementToBeClickable(gfit.lnk_Devices)).click();
		Log.info("Click action performed on main Devices link...");
		Thread.sleep(2500);
		
		if(gfit.btn_GoogleFit_Sync.isDisplayed()==false) {
	        wait.until(ExpectedConditions.elementToBeClickable(gfit.btn_Connect_GoogleFit)).click();
	        Log.info("Click action perfromed on GoogleFit link...");
			// Google login				
			Log.info("Switched to next opened google window...");
			
	        Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(gfit.txt_Gmail_Email)).sendKeys("sunilkumartestingfitbase@gmail.com");
		    Log.info("Gmail username text field has appeared and entered username... "+"sunilkumartestingfitbase@gmail.com");
			Thread.sleep(3000);

			wait.until(ExpectedConditions.elementToBeClickable(gfit.btn_GmailEmail_Next)).click();	    
			Log.info("Google next button has appeared and click action performed on it... ");   
			Thread.sleep(3000);

	        wait.until(ExpectedConditions.elementToBeClickable(gfit.txt_Gmail_Password)).sendKeys("Testing@123");
	    	Log.info("Gmail password text field has appeared and entered password... ");  
	        Thread.sleep(3000);

	        wait.until(ExpectedConditions.elementToBeClickable(gfit.btn_GmailPassword_Next)).click();
			Log.info("Google next button has appeared and click action performed on it... ");
		    Thread.sleep(3000);
		    
		    wait.until(ExpectedConditions.elementToBeClickable(gfit.btn_FitbaseGmailAllow)).click();
			Log.info("Google allow fitbase and allow to click action performed on button... ");
		    Thread.sleep(3000);
		    
			Actions action = new Actions(driver);
			action.sendKeys(Keys.PAGE_DOWN).build().perform();
			
			wait.until(ExpectedConditions.elementToBeClickable(gfit.btn_AuthenticationPage_GoogleFit)).click();
			Thread.sleep(20000);
	        }else {
				Log.info("GoogleFit device has already connected...");
			}
			
			wait.until(ExpectedConditions.elementToBeClickable(gfit.btn_GoogleFit_Sync)).click();
			Log.info("Click action performed on GoogleFit Sync button...");
			Thread.sleep(3000);
			Assert.assertTrue(gfit.msg_GoogleFit_LastSync.isDisplayed());
			Log.info("Connecting & syncing with GoogleFit device has verified successfully...");
			Thread.sleep(5500);
			
			JavascriptExecutor js = (JavascriptExecutor) driver; 
			//This will scroll up the page  
			js.executeScript("window.scrollBy(0,-550)");
			Thread.sleep(2000);
	
	}
}
