package com.fitbasewebchecklist.qa.actions;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fitbasewebchecklist.qa.pages.LoginPage;
import com.fitbasewebchecklist.qa.pages.SignUpPage;
import com.fitbasewebchecklist.qa.utilities.Log;
import com.fitbasewebchecklist.qa.utilities.TestUtil;

import org.openqa.selenium.support.ui.Select;

public class SignUpAction {
	public static WebDriver driver;

	public static void fitbaseSignUpflow(WebDriver driver, String fName, String lName, String pwsd) throws Exception {
		
		SignUpPage signUp = new SignUpPage(driver);
		LoginPage login = new LoginPage(driver);
		//SoftAssert softAssert = new SoftAssert();
		
		WebDriverWait wait = new WebDriverWait(driver, 40);

		//Assert.assertTrue(login.btn_LoginAndSignUp.isEnabled());
		wait.until(ExpectedConditions.elementToBeClickable(login.btn_LoginAndSignUp)).click();
		Log.info("Login or Signup button has displayed and click action performed on it...");
		
		//Assert.assertTrue(signUp.tab_SignUpInLoginForm.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(signUp.tab_SignUpInLoginForm)).click();
		Log.info("Click action performed on sign up tab...");
		
		//Assert.assertTrue(signUp.txt_FirstName.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(signUp.txt_FirstName)).sendKeys(fName);
		Log.info("First name field has displayed and entered first name... "+fName);
		
		//Assert.assertTrue(signUp.txt_LastName.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(signUp.txt_LastName)).sendKeys(lName);
		Log.info("Last name field has displayed and entered last name... "+lName);
				
		//Assert.assertTrue(signUp.txt_EmalAddress.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(signUp.txt_EmalAddress)).sendKeys(TestUtil.generateRandomString()+"@mailinator.com");
		//wait.until(ExpectedConditions.elementToBeClickable(signUp.txt_EmalAddress)).sendKeys("inlpriya2@gmail.com");
		Log.info("Email address field has displayed and entered email address... ");
		
	    //Assert.assertTrue(signUp.txt_Password.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(signUp.txt_Password)).sendKeys(pwsd);
		Log.info("Password field has displayed and entered password..."+pwsd);
		
	    //Assert.assertTrue(signUp.checkbox_Terms.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(signUp.checkbox_Terms)).click();
		Log.info("Click action performed on terms of use checkbox...");
		
		Assert.assertTrue(signUp.btn_SignUp.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_SignUp)).click();
		Log.info("Sign Up button has displayed and click action performed on sign up button...");
		
		//softAssert.assertAll();
		try {
			Thread.sleep(2000);
			if(signUp.err_EmailAddAlreadyExists.isDisplayed()==true) {
						
			//When we provide the already existing Email address
        	wait.until(ExpectedConditions.elementToBeClickable(signUp.txt_EmalAddress)).clear();
        	Log.warn("Entered already existing email address..., hence clearing this data and entering new email address....");
        	wait.until(ExpectedConditions.elementToBeClickable(signUp.txt_EmalAddress)).sendKeys(TestUtil.generateRandomString()+"@mailinator.com");
    		Log.info("Enter new email address...");
        	wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_SignUp)).click();
        	Log.info("Click action performed on Sign up button again...");
			} else {
				Log.info("Email is not duplicated...");
			}} catch(Exception e) {
		  
		     // e.printStackTrace();
	       }
		 finally {
			    driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
			}
	}
	
	//####### Skipping health details ########
	public static void skipHealthDetails(WebDriver driver) {
		SignUpPage skipHealthDetails = new SignUpPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 50);
		
		wait.until(ExpectedConditions.elementToBeClickable(skipHealthDetails.btn_Skip)).click();
		Log.info("Skip button has displayed and click action performed on it...");
		wait.until(ExpectedConditions.elementToBeClickable(skipHealthDetails.btn_NotNow)).click();
		Log.info("Not now button has displayed and click action perfomed on it...");
	}
	
	//#### Verify dashboard ########
	public static void verifyDashboard(WebDriver driver) throws InterruptedException {
		Thread.sleep(4000);		

		String ActualValue="Dashboard";
		String ExpectedValue=driver.getTitle();
		Assert.assertEquals(ActualValue, ExpectedValue);		
		Log.info("Page title has verified after signing up...");
	}
	
	//###### Filling health details #######
	public static void fillHealthDetails(WebDriver driver) throws InterruptedException {
		SignUpPage fillHealthDetails = new SignUpPage(driver);
	    WebDriverWait wait=new WebDriverWait(driver, 80);
		Thread.sleep(4000);	
			
		Assert.assertEquals(driver.getTitle(), "Fitbase : Health profile setup");
		Log.info("Health profile setup page has verified..");
	
		wait.until(ExpectedConditions.elementToBeClickable(fillHealthDetails.btn_Height_Feet)).sendKeys("5");
		Log.info("Feet value has selected... from dropdown");
		wait.until(ExpectedConditions.elementToBeClickable(fillHealthDetails.btn_Height_Inches)).sendKeys("2");
		Log.info("Inches value has selected... from dropdown");
		wait.until(ExpectedConditions.elementToBeClickable(fillHealthDetails.btn_Weight_LBS)).sendKeys("777");
		Log.info("Enterted weight value in LBS...");
		
		Select selectMonth = new Select(fillHealthDetails.drp_Months);
		selectMonth.selectByIndex(2);
		Log.info("Selected month value from dropdown...");
		
		Select selectDate = new Select(fillHealthDetails.drp_Date);
		selectDate.selectByValue("20");
		Log.info("Selected date value from dropdown...");
		
		Select selectYear = new Select(fillHealthDetails.drp_Year);
		selectYear.selectByVisibleText("1995");
		Log.info("Selected year value from dropdown...");
		
		Select selectGender = new Select(fillHealthDetails.drp_Gender);
		selectGender.selectByIndex(1);
		Log.info("Selected year value from dropdown...");
		
		Select selectActivityLevel = new Select(fillHealthDetails.drp_ActivityLevel);
		selectActivityLevel.selectByIndex(3);
		Log.info("Selected activity level value from dropdown...");
		
		wait.until(ExpectedConditions.elementToBeClickable(fillHealthDetails.btn_Next)).click();
		Log.info("Click action performed on Next button in health details set up page...");
		
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(fillHealthDetails.btn_WorkoutActivity_Next)).click();
		Log.info("Click action performed on Next button in workout activity page in health details set up page...");
		Thread.sleep(3000);

		wait.until(ExpectedConditions.elementToBeClickable(fillHealthDetails.btn_GoalTab_Later)).click();
		Log.info("Click action performed on Later button on Goal page in health details set up page...");
		
	}
	
}
