package com.fitbasewebchecklist.qa.actions;


import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import com.fitbasewebchecklist.qa.pages.LoginPage;
import com.fitbasewebchecklist.qa.pages.SignUpPage;
import com.fitbasewebchecklist.qa.utilities.Log;


public class LoginAction {
	public static WebDriver driver;
	
	public static void fitbaseProdNormalLogin(WebDriver driver, String uname, String pswd) throws Exception {

		LoginPage login = new LoginPage(driver);
		SignUpPage signUp=new SignUpPage(driver);
		SoftAssert softAssert = new SoftAssert();
        WebDriverWait wait = new WebDriverWait(driver, 70);
        
		softAssert.assertTrue(login.btn_LoginAndSignUp.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(login.btn_LoginAndSignUp)).click();
		Log.info("Signup or Login button has displayed and click action has performed on it");
		
		wait.until(ExpectedConditions.elementToBeClickable(login.txt_UserName)).clear();
		wait.until(ExpectedConditions.elementToBeClickable(login.txt_UserName)).sendKeys(uname);
		Log.info("Username field has displayed and entered username "+uname);
		
		softAssert.assertTrue(login.txt_Password.isDisplayed());
		login.txt_Password.clear();
		login.txt_Password.sendKeys(pswd);
		Log.info("Password field has displayed and entered password "+pswd);
		
		softAssert.assertTrue(login.btn_Submit.isDisplayed());
		login.btn_Submit.click();
		Log.info("Click action performed on Submit button");


		 driver.manage().timeouts().implicitlyWait(60,TimeUnit.MILLISECONDS);
		    try {
				Thread.sleep(3000);
				
	        if(signUp.btn_Skip.isDisplayed()==true) {
	        	
	        	//Actions actions = new Actions(driver);
	    		//actions.sendKeys(Keys.PAGE_DOWN).perform();
	        	//This will scroll down the page by 1000 pixel vertical 
		    	JavascriptExecutor js = (JavascriptExecutor) driver; 
				js.executeScript("window.scrollBy(0,2000)");
				Thread.sleep(3000);
				
				wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_Skip)).click();
				Log.info("Health details are not filled for this user, click action performed on skip button...");
		        
				wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_NotNow)).click();
	            Log.info("Click action performed on not now button on health details page...");
	           } 
	         
	        else {
	        	  Log.info("Health details are filled...");
	           }}
		    catch(Exception e) {
		    } 
		    
		    finally
			{
			    driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
			}
		    Thread.sleep(9000);
		    String ActualValue="Dashboard";
			String ExpectedValue=driver.getTitle();
			Assert.assertEquals(ActualValue, ExpectedValue);
		    Log.info("Page title verified successfully after login...");
	   
		    Assert.assertTrue(driver.getTitle().equals("Dashboard"));
		    Log.info("VERIFIED PAGE TITLE SUCCESSFULLY.....");

		
		softAssert.assertAll();
		Log.info("Verify all the assertions kept in this action calls of login");
	}

	public static void fitbaseNormalLogin(WebDriver driver, String uname, String pswd) throws Exception {

		LoginPage login = new LoginPage(driver);
		SignUpPage signUp=new SignUpPage(driver);
		SoftAssert softAssert = new SoftAssert();
        WebDriverWait wait = new WebDriverWait(driver, 70);
        Thread.sleep(3000);
		softAssert.assertTrue(login.btn_LoginAndSignUp.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(login.btn_LoginAndSignUp)).click();
		Log.info("Signup or Login button has displayed and click action has performed on it");
		
		wait.until(ExpectedConditions.elementToBeClickable(login.txt_UserName)).clear();
		wait.until(ExpectedConditions.elementToBeClickable(login.txt_UserName)).sendKeys(uname);
		Log.info("Username field has displayed and entered username "+uname);
		
		softAssert.assertTrue(login.txt_Password.isDisplayed());
		login.txt_Password.clear();
		login.txt_Password.sendKeys(pswd);
		Log.info("Password field has displayed and entered password "+pswd);
		
		softAssert.assertTrue(login.btn_Submit.isDisplayed());
		login.btn_Submit.click();
		Log.info("Click action performed on Submit button");


		 driver.manage().timeouts().implicitlyWait(60,TimeUnit.MILLISECONDS);
		    try {
				Thread.sleep(3000);
				
	        if(signUp.btn_Skip.isDisplayed()==true) {
	        	
	        	//Actions actions = new Actions(driver);
	    		//actions.sendKeys(Keys.PAGE_DOWN).perform();
	        	//This will scroll down the page by 1000 pixel vertical 
		    	JavascriptExecutor js = (JavascriptExecutor) driver; 
				js.executeScript("window.scrollBy(0,2000)");
				Thread.sleep(3000);
				
				wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_Skip)).click();
				Log.info("Health details are not filled for this user, click action performed on skip button...");
		        
				wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_NotNow)).click();
	            Log.info("Click action performed on not now button on health details page...");
	           } 
	         
	        else {
	        	  Log.info("Health details are filled...");
	           }}
		    catch(Exception e) {
		    } 
		    
		    finally
			{
			    driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
			}
		    Thread.sleep(9000);
		    String ActualValue="Dashboard";
			String ExpectedValue=driver.getTitle();
			Assert.assertEquals(ActualValue, ExpectedValue);
		    Log.info("Page title verified successfully after login...");
	   
		    Assert.assertTrue(driver.getTitle().equals("Dashboard"));
		    Log.info("VERIFIED PAGE TITLE SUCCESSFULLY.....");

		
		softAssert.assertAll();
		Log.info("Verify all the assertions kept in this action calls of login");
	}
	
	public static void fitbaseFacebookLogin(WebDriver driver, String facebookUserName, String facebookPassword) throws InterruptedException {
		LoginPage facebookLogin = new LoginPage(driver);
		SignUpPage signUp = new SignUpPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 80);

		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.btn_LoginAndSignUp)).click();
		Log.info("Signup or Login button has displayed and click action has performed on it");
        Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.tab_FacebookLogin)).click();
		Log.info("Click action performed on facebook tab...");
		
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> it=windows.iterator();
		String parentWindow=it.next();
		String childWindow=it.next();
		
		driver.switchTo().window(childWindow);
		Log.info("Facebook login window has opened...");
		
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.txt_Facebook_UserName)).sendKeys(facebookUserName);
		Log.info("Entered facebook login username... "+facebookUserName);
		
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.txt_Facebook_Password)).sendKeys(facebookPassword);
		Log.info("Entered facebook login password... "+facebookPassword);
		
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.btn_FacebookLogin)).click();
		Log.info("Click action performed on facebook login button...");
		
		//driver.findElement(By.xpath("//*[@id='platformDialogForm']/div[3]/div/table/tbody/tr/td[2]/button[2]")).click();

		driver.switchTo().window(parentWindow);
		
        driver.manage().timeouts().implicitlyWait(60,TimeUnit.MILLISECONDS);
        Log.info("Implicit wait applied on driver to wait for 6 seconds...");
        
        try {
			Thread.sleep(3000);
        
		if(signUp.btn_Skip.isDisplayed()==true) {
	    	JavascriptExecutor js = (JavascriptExecutor) driver; 
			//This will scroll down the page by 2000 pixel vertical 
			js.executeScript("window.scrollBy(0,2000)");
			
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_Skip)).click();
	        Log.info("Health details are not filled, clicking on Skip button..."); 
			wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_NotNow)).click();
            Log.info("Click action performed on not now button... on health details page..");
			
		   } else {
        	   Log.info("Health details are already filled... hence redirecting to dashboard right after login...");
           }
	       }
	    catch(Exception e) {
	    	
	    } 
	    
	    finally
		{
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		}
	    
	    Thread.sleep(9000);
	    String ActualValue="Dashboard";
		String ExpectedValue=driver.getTitle();
		Assert.assertEquals(ActualValue, ExpectedValue);
	    Log.info("Page title verified successfully after login...");        
	}
	
	public static void fitbaseProdFacebookLogin(WebDriver driver, String facebookUserName, String facebookPassword) throws InterruptedException {
		LoginPage facebookLogin = new LoginPage(driver);
		SignUpPage signUp = new SignUpPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 80);

		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.btn_LoginAndSignUp)).click();
		Log.info("Signup or Login button has displayed and click action has performed on it");
        Thread.sleep(2500);
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.tab_FacebookLogin)).click();
		Log.info("Click action performed on facebook tab...");
		
		Set<String> windows = driver.getWindowHandles();
		Iterator<String> it=windows.iterator();
		String parentWindow=it.next();
		String childWindow=it.next();
		
		driver.switchTo().window(childWindow);
		Log.info("Facebook login window has opened...");
		
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.txt_Facebook_UserName)).sendKeys(facebookUserName);
		Log.info("Entered facebook login username... "+facebookUserName);
		
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.txt_Facebook_Password)).sendKeys(facebookPassword);
		Log.info("Entered facebook login password... "+facebookPassword);
		
		wait.until(ExpectedConditions.elementToBeClickable(facebookLogin.btn_FacebookLogin)).click();
		Log.info("Click action performed on facebook login button...");
		driver.switchTo().window(parentWindow);
		
        driver.manage().timeouts().implicitlyWait(60,TimeUnit.MILLISECONDS);
        Log.info("Implicit wait applied on driver to wait for 6 seconds...");
        
        try {
			Thread.sleep(3000);
        
		if(signUp.btn_Skip.isDisplayed()==true) {
	    	JavascriptExecutor js = (JavascriptExecutor) driver; 
			//This will scroll down the page by 2000 pixel vertical 
			js.executeScript("window.scrollBy(0,2000)");
			
			Thread.sleep(3000);
			wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_Skip)).click();
	        Log.info("Health details are not filled, clicking on Skip button..."); 
			wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_NotNow)).click();
            Log.info("Click action performed on not now button... on health details page..");
			
		   } else {
        	   Log.info("Health details are already filled... hence redirecting to dashboard right after login...");
           }
	       }
	    catch(Exception e) {
	    	
	    } 
	    
	    finally
		{
		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
		}
	    
	    Thread.sleep(9000);
	    String ActualValue="Dashboard";
		String ExpectedValue=driver.getTitle();
		Assert.assertEquals(ActualValue, ExpectedValue);
	    Log.info("Page title verified successfully after login...");        
	}
	
public static void fitbaseProdGoogleLogin(WebDriver driver, String gmailUserName, String gmailPassword) throws Exception{
		
		LoginPage googleLogin = new LoginPage(driver);
		SignUpPage signUp = new SignUpPage(driver);
        WebDriverWait wait = new WebDriverWait(driver, 77);
        
        googleLogin.btn_LoginAndSignUp.click();
        Thread.sleep(2500);
        wait.until(ExpectedConditions.elementToBeClickable(googleLogin.tab_GoogleLogin)).click();
		Log.info("Google tab has displayed and click action has performed on it...");
        
        // Window handling
        Set<String> window = driver.getWindowHandles();	
        Iterator<String> it= window.iterator();

        String parentWindow = it.next();
        String childWindow = it.next();
        
        driver.switchTo().window(childWindow);				
		Log.info("Switched to next opened google window...");
		
        Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(googleLogin.txt_Gmail_Email)).sendKeys(gmailUserName);
	    Log.info("Gmail username text field has appeared and entered username... "+gmailUserName);
		Thread.sleep(3000);

		wait.until(ExpectedConditions.elementToBeClickable(googleLogin.btn_GmailEmail_Next)).click();	    
		Log.info("Google next button has appeared and click action performed on it... ");   
		Thread.sleep(3000);

        wait.until(ExpectedConditions.elementToBeClickable(googleLogin.txt_Gmail_Password)).sendKeys(gmailPassword);
    	Log.info("Gmail password text field has appeared and entered password... "+gmailPassword);  
        Thread.sleep(3000);

        wait.until(ExpectedConditions.elementToBeClickable(googleLogin.btn_GmailPassword_Next)).click();
		Log.info("Google next button has appeared and click action performed on it... ");
	   Thread.sleep(2000);
  //  wait.until(ExpectedConditions.elementToBeClickable(googleLogin.GmailAllowButton)).click();
//
   Thread.sleep(2000);
    driver.switchTo().window(parentWindow);
    driver.manage().timeouts().implicitlyWait(60,TimeUnit.MILLISECONDS);
    Thread.sleep(12000);
//    String ActualValue="Fitbase - Wellness. Anywhere. Anytime.";
//	String ExpectedValue=driver.getTitle();
//	Assert.assertEquals(ActualValue, ExpectedValue);
//    Log.info("Page title verified successfully after login...");
//	    try {
//			Thread.sleep(3000);
//			
//        if(signUp.btn_Skip.isDisplayed()==true) {
//	    	JavascriptExecutor js = (JavascriptExecutor) driver; 
//			//This will scroll down the page by 1000 pixel vertical 
//			js.executeScript("window.scrollBy(0,2000)");
//			Thread.sleep(3000);
//			
//			wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_Skip)).click();
//			Log.info("Health details are not filled for this user, click action performed on skip button...");
//	        
//			wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_NotNow)).click();
//            Log.info("Click action performed on not now button on health details page...");
//           } 
//         
//        else {
//        	  Log.info("Health details are filled...");
//           }}
//	    catch(Exception e) {
//	    } 
//	    
//	    finally
//		{
//		    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
//		}
//	    Thread.sleep(9000);
//	    String ActualValue="Dashboard";
//		String ExpectedValue=driver.getTitle();
//		Assert.assertEquals(ActualValue, ExpectedValue);
//	    Log.info("Page title verified successfully after login...");
   }
public static void fitbaseGoogleLogin(WebDriver driver, String gmailUserName, String gmailPassword) throws Exception{
	
	LoginPage googleLogin = new LoginPage(driver);
	SignUpPage signUp = new SignUpPage(driver);
    WebDriverWait wait = new WebDriverWait(driver, 77);
    
    googleLogin.btn_LoginAndSignUp.click();
    Thread.sleep(2500);
    wait.until(ExpectedConditions.elementToBeClickable(googleLogin.tab_GoogleLogin)).click();
	Log.info("Google tab has displayed and click action has performed on it...");
    
    // Window handling
    Set<String> window = driver.getWindowHandles();	
    Iterator<String> it= window.iterator();

    String parentWindow = it.next();
    String childWindow = it.next();
    
    driver.switchTo().window(childWindow);				
	Log.info("Switched to next opened google window...");
	
    Thread.sleep(3000);
	wait.until(ExpectedConditions.elementToBeClickable(googleLogin.txt_Gmail_Email)).sendKeys(gmailUserName);
    Log.info("Gmail username text field has appeared and entered username... "+gmailUserName);
	Thread.sleep(3000);

	wait.until(ExpectedConditions.elementToBeClickable(googleLogin.btn_GmailEmail_Next)).click();	    
	Log.info("Google next button has appeared and click action performed on it... ");   
	Thread.sleep(3000);

    wait.until(ExpectedConditions.elementToBeClickable(googleLogin.txt_Gmail_Password)).sendKeys(gmailPassword);
	Log.info("Gmail password text field has appeared and entered password... "+gmailPassword);  
    Thread.sleep(3000);

    wait.until(ExpectedConditions.elementToBeClickable(googleLogin.btn_GmailPassword_Next)).click();
	Log.info("Google next button has appeared and click action performed on it... ");
   Thread.sleep(3000);
  wait.until(ExpectedConditions.elementToBeClickable(googleLogin.GmailAllowButton)).click();
//
//Thread.sleep(2000);
driver.switchTo().window(parentWindow);
driver.manage().timeouts().implicitlyWait(60,TimeUnit.MILLISECONDS);
Thread.sleep(12000);
//String ActualValue="Fitbase - Wellness. Anywhere. Anytime.";
//String ExpectedValue=driver.getTitle();
//Assert.assertEquals(ActualValue, ExpectedValue);
//Log.info("Page title verified successfully after login...");
//    try {
//		Thread.sleep(3000);
//		
//    if(signUp.btn_Skip.isDisplayed()==true) {
//    	JavascriptExecutor js = (JavascriptExecutor) driver; 
//		//This will scroll down the page by 1000 pixel vertical 
//		js.executeScript("window.scrollBy(0,2000)");
//		Thread.sleep(3000);
//		
//		wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_Skip)).click();
//		Log.info("Health details are not filled for this user, click action performed on skip button...");
//        
//		wait.until(ExpectedConditions.elementToBeClickable(signUp.btn_NotNow)).click();
//        Log.info("Click action performed on not now button on health details page...");
//       } 
//     
//    else {
//    	  Log.info("Health details are filled...");
//       }}
//    catch(Exception e) {
//    } 
//    
//    finally
//	{
//	    driver.manage().timeouts().implicitlyWait(3000, TimeUnit.SECONDS);
//	}
//    Thread.sleep(9000);
//    String ActualValue="Dashboard";
//	String ExpectedValue=driver.getTitle();
//	Assert.assertEquals(ActualValue, ExpectedValue);
//    Log.info("Page title verified successfully after login...");
}

public static void fitbaseForgotPassword(WebDriver driver, String emailAddress) throws Exception{
	
	LoginPage login = new LoginPage(driver);
    WebDriverWait wait = new WebDriverWait(driver, 8);
    
	//Assert.assertTrue(login.btn_LoginAndSignUp.isDisplayed());
	wait.until(ExpectedConditions.elementToBeClickable(login.btn_LoginAndSignUp)).click();
	Log.info("Signup or Login button has displayed and click action has performed on it...");
	
	wait.until(ExpectedConditions.elementToBeClickable(login.lnk_fotgotPassword)).click();
	Log.info("Click action performed on forgot password link...");
	Thread.sleep(2000);
	wait.until(ExpectedConditions.elementToBeClickable(login.txt_forgotPassword)).sendKeys(emailAddress);
	Log.info("Entered email address is"+emailAddress+" to test forgot password email functionality...");
	Thread.sleep(1000);
	wait.until(ExpectedConditions.elementToBeClickable(login.btn_forgotPassword)).click();
	Log.info("Click action performed on forgot password link...");
	Thread.sleep(5000);
	wait.until(ExpectedConditions.elementToBeClickable(login.msg_forgotPasswordSuccessMessage)).isDisplayed();
	Log.info("Forgot password success message has been displayed...");
	
//	((JavascriptExecutor)driver).executeScript("window.open()");
//    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
//    driver.switchTo().window(tabs.get(1));
//    driver.get("https://accounts.google.com/signin");	
//    
//	wait.until(ExpectedConditions.elementToBeClickable(login.txt_Gmail_Email)).sendKeys("DailyChecklistTestReport@gmail.com");
//    Log.info("Gmail username text field has appeared and entered username... ");
//
//	wait.until(ExpectedConditions.elementToBeClickable(login.btn_GmailEmail_Next)).click();	    
//	Log.info("Google next button has appeared and click action performed on it... ");   
//
//    wait.until(ExpectedConditions.elementToBeClickable(login.txt_Gmail_Password)).sendKeys("Testing@12345");
//	Log.info("Gmail password text field has appeared and entered password... ");  
//
//    wait.until(ExpectedConditions.elementToBeClickable(login.btn_GmailPassword_Next)).click();
//	Log.info("Google next button has appeared and click action performed on it... ");
//    Thread.sleep(3000);
//    	
//    driver.get("https://mail.google.com/mail/u/0/#inbox");
//    Thread.sleep(2000);
//
//    // Search forgot password mail
//    driver.findElement(By.xpath("//input[@aria-label='Search mail']")).sendKeys("Reset your fitbase password");
//    driver.findElement(By.xpath("//input[@aria-label='Search mail']")).sendKeys(Keys.RETURN);
//    Log.info("Enter 'Reset your fitbase password' into the search box and click action performed to enter...");
//    Thread.sleep(4000);
//        
//    //Click on first mail
//    driver.findElement(By.xpath("//*[@id=\":mg\"]/span")).click();
//    // driver.findElement(By.xpath("//*[@id=':ma']/td[5]")).click();
//    Thread.sleep(2000);
//    Log.info("Click action performed on first mail....");
//    
//    //Forfot password heading
//    Assert.assertTrue(driver.findElement(By.xpath("//div[@class=\"nH\"]//div[@class=\"ha\"]//h2")).isDisplayed());
//    Log.info("'Reset your fitbase password' heading has displayed and verified it successfully...");
//
//    //Check last minute text
//    Assert.assertTrue(driver.findElement(By.xpath("//span[contains(text(),'minutes ago')]")).isDisplayed());
//    Log.info("'Last minutes ago' text has displayed and verified successfully...");
//    
// /*   try{
//    // Reset your password text
//    Assert.assertTrue(driver.findElement(By.xpath("//td[contains(text(),'Reset your password')]")).isDisplayed());
//    Log.info("'Reset your password' text has displayed and verified successfully...");
//    }catch(Exception e) {
//    	Log.info("There are no multiple mails founds...");
//    }finally {
//    	driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
//    }
//*/    //Logout
//    Thread.sleep(2000);
//    driver.findElement(By.xpath("//a[@href='https://accounts.google.com/SignOutOptions?hl=en&continue=https://mail.google.com/mail&service=mail']")).click();
//    Log.info("Click action performed on menu... from gmail acoount...");
//    driver.findElement(By.xpath("//a[contains(text(),'Sign out')]")).click();
//    Thread.sleep(2000);
//    Log.info("Click action performed on sign out button... from gmail acoount...");
//    Thread.sleep(3000);
//
//    try{
//    driver.switchTo().alert().accept();
//    Log.info("Alert has displayed & accepted it to logout...");
//    }catch(Exception e) {
//    	Log.info("No alert displayed to accept it to logout...");
//    }finally {
//    	driver.manage().timeouts().implicitlyWait(15000, TimeUnit.MILLISECONDS);
//    }
//
//    Thread.sleep(2500);
//    Assert.assertTrue(driver.getTitle().equals("Gmail"));
//    Log.info("Logged-out successfully... page has verified successfully...");


}
}

