package com.fitbasewebchecklist.qa.actions;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fitbasewebchecklist.qa.pages.LiveSessionsPage;
import com.fitbasewebchecklist.qa.utilities.Log;



public class LiveSessionsAction {
	
	public WebDriver driver;

	public static void enrollApaidSessionWithSavedCards(WebDriver driver) throws Exception {
		LiveSessionsPage bookAliveSession = new LiveSessionsPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.lnk_LiveSessions)).click();
		Log.info("Click action perfomed on Live Sessions main module...");
        Thread.sleep(2000);
               
		Select drpPrice = new Select(bookAliveSession.drp_SortByPrice);
		drpPrice.selectByIndex(2);
		Log.info("Selected price dropdown value... from filter");
		Thread.sleep(2000);
		if(bookAliveSession.msg_HelpMessageWhenThereAreNoWorkoutsAvailable.isDisplayed()==false) {
	        
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.lnk_WorkoutSession)).click();
		Log.info("Verified whether live sessions are avaiable or not, found that live sessions are available...");
		Log.info("Click action performed on live sessions link...");
		Thread.sleep(2000);

		JavascriptExecutor js = (JavascriptExecutor) driver; 
		//This will scroll down the page by 1000 pixel vertical 
		js.executeScript("window.scrollBy(0,500)");
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.btn_EnrollNowInSessionPreviewPage)).click();
		Log.info("Click action performed on Enroll now in session preview page...");
		Thread.sleep(1000);
				
		try {
			
		if(bookAliveSession.btn_ConflictConfirmation.isDisplayed()==true) {
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.btn_ConflictConfirmation)).click();
			
		Log.info("Click action performed on okay button on conflict pop-up...");
		} else {
			Log.info("NO CONFLICT POP-UP FOUND...");
		}
		} catch (Exception  e) {
			//e.printStackTrace();
		}
		finally
		{
		    driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
		}
		
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.btn_EnrollNowInConfirmationPopUp)).click();
		Log.info("Click action performed on Enroll now button on confirmation pop-up...");	
	
		js.executeScript("window.scrollBy(0,10000)");
		Log.info("Scroll down till the end of the page");
		
		Thread.sleep(3000);
		if(bookAliveSession.tab_SavedCards.isDisplayed()==true) {
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.btn_PayInCheckoutPage)).click();
		Log.info("Click on Pay now button in Saved cards tab...");
		Thread.sleep(12000);
		Assert.assertTrue(bookAliveSession.paymentSuccessPage.isDisplayed());
		Log.info("Payment has done successfully and page has verified successfully...");
		
		Thread.sleep(1500);
		} else {
			
			Log.info("No saved card displayed... to check testcase - payment with saved cards....");
		}		
    }else {
		Log.info("There are no live sessions available to verify....");

    }
	}

	////////////////////////// Pay with PayPal /////////////////////////////////
	
	public static void enrollApadiSessionWithPayPal(WebDriver driver) throws InterruptedException {
	
		LiveSessionsPage enrollApaidSessionWithPayPal = new LiveSessionsPage(driver);
		
		WebDriverWait wait=new WebDriverWait(driver, 60);
		
		wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.lnk_LiveSessions)).click();
		Log.info("Click action perfomed on Live Sessions main module...");
        Thread.sleep(4000);
        
        wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.Clickonfitlericon)).click();
		Log.info("Click action performed on Live Session Filter Icon...");
		Thread.sleep(5000);
		
		 wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.ClickonsessionPrice)).click();
		Log.info("Click action performed on session Price drop down...");
		Thread.sleep(4000);
	        
		Select drpPrice = new Select(LiveSessionsPage.ClickonsessionPrice);
		drpPrice.selectByVisibleText("$10 - $100");
		Log.info("Selected price dropdown from filter...");
		Thread.sleep(4000);

		driver.findElement(By.xpath("/html/body/jhi-main/jhi-livesession/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/form/button")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-livesession/div/div[2]/div/div[2]/div[2]/div[2]/div/div/div/a/div/div[2]")).click();
		JavascriptExecutor js = (JavascriptExecutor) driver; 
	
//		if(enrollApaidSessionWithPayPal.msg_HelpMessageWhenThereAreNoWorkoutsAvailable.isDisplayed()==false) {
//	        
//		wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.lnk_WorkoutSession)).click();
//        Log.info("Verified whether any paid live sessions are available or not, found that live sessions are available...");
//		
//		JavascriptExecutor js = (JavascriptExecutor) driver; 
//		//This will scroll down the page by 1000 pixel vertical 
//		js.executeScript("window.scrollBy(0,500)");
//		Thread.sleep(2000);
//
//		wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.btn_EnrollNowInSessionPreviewPage)).click();
//		Log.info("Click action performed on Enroll now in session preview page...");
//		Thread.sleep(1000);
//				
//		try {
//			if(enrollApaidSessionWithPayPal.btn_ConflictConfirmation.isDisplayed()==true) {
//		wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.btn_ConflictConfirmation)).click();
//		Log.info("Click action performed on okay button on conflict pop-up...");
//			}	else {
//				Log.info("NO CONFLICT POP-UP FOUND...");
//			}
//		} catch (Exception  e) {
//			// TODO: handle exception
//			//e.printStackTrace();
//		}
//		finally
//		{
//		    driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
//		}
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-livesessiondetails/div/div[2]/div/div[1]/div[2]/div/div/div[3]/div/div[2]/div/button\r\n" + "")).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.btn_EnrollNowInConfirmationPopUp)).click();
        Log.info("Click action perfomed on Enroll button on confirmation pop-up...");
        Thread.sleep(1000);
        
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div/div/div/div/div/div[2]/div[3]/div[1]/div/div/div/div/p/button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\'discountCoupon\']")).sendKeys("coupon123");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\'demo\']/form/div/div[1]/button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div/div/div/div/div/div[2]/div[4]/button")).click();
		Thread.sleep(10000);
		
		if(enrollApaidSessionWithPayPal.tab_PayPal.isDisplayed()==true) {
			Thread.sleep(4000);
	
			wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.tab_PayPal)).click();
			Log.info("Click action perfomed on paypal tab...");
			js.executeScript("window.scrollBy(0,10000)");
			Thread.sleep(3000);
			
     		enrollApaidSessionWithPayPal.btn_PayButtonUnderPayPalTab.click();
			Log.info("Click action perfomed on Pay with paypal button under paypal tab...");			
			Thread.sleep(8000);
			
		//	driver.navigate().refresh();
		//	Thread.sleep(1000);
			
			//driver.findElement(By.xpath("//*[@id=\'loginSection\']/div/div[2]/a")).click();
			//Paypal login to pay with PayPal
			//Thread.sleep(3000);
			
			wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.txt_PayPal_EmailAddress)).sendKeys("vijaykumarkalpagur@gmail.com");
			Log.info("Paypal login page has displayed and entered paypal email address...");
			Thread.sleep(2000);
			
			wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.btn_Next_PayPalEmailAddress)).click();
			Log.info("Click action performed on next button on paypal login page...");
			Thread.sleep(2000);
			
			wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.txt_PayPal_Password)).sendKeys("Testing@123");
			Log.info("Paypal login page has displayed and entered paypal password...");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.btn_PayPal_Login)).click();
			Log.info("Click action performed on PayPal login button...");
			
			Thread.sleep(15000);
			js.executeScript("window.scrollBy(0,10000)");
			
			Thread.sleep(3000);
			
			wait.until(ExpectedConditions.elementToBeClickable(enrollApaidSessionWithPayPal.btn_PayPalPayment_Continue)).click();
			Log.info("Click action performed on continue button on Paypal conformation page...");
			
			js.executeScript("window.scrollBy(0,10000)");
			
			Thread.sleep(10000);
			Assert.assertTrue(enrollApaidSessionWithPayPal.paymentSuccessPage.isDisplayed());
			Log.info("Payment has done successfully and page has verified successfully...");
			
			Thread.sleep(1500);
	    }
	      
	 else {
			Log.info("There are no live sessions available to verify....");
	  }
}

	///////////////////////////// Pay with Credit / Debit card ///////////////////////
	
	public static void enrollApaidSessionWithCreditOrDebitCard(WebDriver driver) throws Exception {
		LiveSessionsPage enrollASessionWithCreditOrDebitCard = new LiveSessionsPage(driver);
		
		WebDriverWait wait=new WebDriverWait(driver, 60);
		
		wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.lnk_LiveSessions)).click();
		Log.info("Click action perfomed on Live Sessions main module...");
        Thread.sleep(2000);
        
        wait.until(ExpectedConditions.elementToBeClickable(LiveSessionsPage.Clickonfitlericon)).click();
		Log.info("Click action performed on Live Session Filter Icon...");
		Thread.sleep(5000);
		
		 wait.until(ExpectedConditions.elementToBeClickable(LiveSessionsPage.ClickonsessionPrice)).click();
		Log.info("Click action performed on session Price drop down...");
		Thread.sleep(3000);
	        
        
		Select drpPrice = new Select(LiveSessionsPage.ClickonsessionPrice);
		drpPrice.selectByVisibleText("$10 - $100");
		Log.info("Selected price dropdown from filter...");
		Thread.sleep(2000);

		driver.findElement(By.xpath("/html/body/jhi-main/jhi-livesession/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/form/button")).click();
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-livesession/div/div[2]/div/div[2]/div[2]/div[2]/div/div/div/a/div/div[2]")).click();
		JavascriptExecutor js = (JavascriptExecutor) driver; 
	
//		if(enrollASessionWithCreditOrDebitCard.msg_HelpMessageWhenThereAreNoWorkoutsAvailable.isDisplayed()==false) {
//    
//		wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.lnk_WorkoutSession)).click();
//		Log.info("Verified whether there are any live sessions available or not, found that there are live sessions available....");
//		Log.info("Paid live session has displayed and click action perfomed on it to preview...");
//		
		//JavascriptExecutor js = (JavascriptExecutor) driver; 
//		//This will scroll down the page by 1000 pixel vertical 
//		js.executeScript("window.scrollBy(0,500)");
//		Thread.sleep(2000);
//
//		wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.btn_EnrollNowInSessionPreviewPage)).click();
//		Log.info("Click action performed on Enroll now in session preview page...");
//		Thread.sleep(5000);
//				
//		try {
//		     if(enrollASessionWithCreditOrDebitCard.btn_ConflictConfirmation.isDisplayed()==true) {
//		    	 
//		wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.btn_ConflictConfirmation)).click();
//		Log.info("Click action performed on okay button on conflict pop-up...");
//		Thread.sleep(1000);
//			} else {
//				Log.info("This session date & time is not overlapping with anyother session date & time... hence no conflict pop-up came...");
//			}
//				
//		} catch (Exception  e) {
//			// TODO: handle exception
//			// e.printStackTrace();
//			Log.info("No conflict pop-up appeared...");
//		}
//		finally
//		{
//		    driver.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
//		}
	
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-livesessiondetails/div/div[2]/div/div[1]/div[2]/div/div/div[3]/div/div[2]/div/button\r\n" + "")).click();
		Thread.sleep(3000);
		wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.btn_EnrollNowInConfirmationPopUp)).click();
        Log.info("Click action perfomed on Enroll button on confirmation pop-up...");
        Thread.sleep(300);
        
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div/div/div/div/div/div[2]/div[3]/div[1]/div/div/div/div/p/button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\'discountCoupon\']")).sendKeys("coupon123");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\'demo\']/form/div/div[1]/button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/jhi-main/jhi-paymentreview/div/div/div/div/div/div[2]/div[4]/button")).click();
		Thread.sleep(4000);
		
		if(enrollASessionWithCreditOrDebitCard.tab_CredirOrDebitCard.isDisplayed()==true) {
			Thread.sleep(2500);
			enrollASessionWithCreditOrDebitCard.tab_CredirOrDebitCard.click();
			Log.info("Click action perfomed on credit card / debit card tab...");
			Thread.sleep(1000);
			wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.txt_CardNumber)).sendKeys("4242 4242 4242 4242 424 242");
//			wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.txt_MonthAndYear)).sendKeys("424");
//			Thread.sleep(2000);
//			wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.txt_CVC)).sendKeys("424");
			Thread.sleep(1000);
			Log.info("Entered Card Number, Month & Year and CVC...");
			//js.executeScript("window.scrollBy(0,10000)");
			Thread.sleep(2000);

			List<WebElement> drr = driver.findElements(By.xpath("//button[@class='pay ng-binding']"));
			//System.out.println(drr.size());
			for(int i3=1; i3<=1; i3++) {
            //driver.findElement(By.xpath("//*[@id=\"accordion\"]/accordion/div/div[2]/div[1]/h4/a")).click();
            Thread.sleep(3000);
							
				//System.out.println(drr.get(i3).getText());
				//if(drr.get(i).getText().contains("Meditation")) {
					drr.get(i3).click();
					Log.info("Click action performed on Pay button under Credit/Debit card tab...");

				//}			
			//wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithCreditOrDebitCard.btn_PayInCheckoutPage)).click();
		

	//	Thread.sleep(6000);
//		String ActualValue="Payment";
//		String ExpectedValue=driver.getTitle();
//		Thread.sleep(6000);
//		// Verifying payment processing page
//		Assert.assertEquals(ActualValue, ExpectedValue);
//		Assert.assertTrue(driver.findElement(By.xpath("/html/body/div[2]/div[2]/div[2]/div[2]/div/div/div/div/div/div/div[2]/table/tbody/tr/td[5]/span")).isDisplayed());
//		Log.info("Payment processing page is verified successfully...");
		
		Thread.sleep(15000);

		wait.until(ExpectedConditions.visibilityOf(enrollASessionWithCreditOrDebitCard.paymentSuccessPage));
		Assert.assertTrue(enrollASessionWithCreditOrDebitCard.paymentSuccessPage.isDisplayed());
		Log.info("Payment success page has appeared and it has verified successfully...");
		Thread.sleep(3000);
		 
		} 
		}
		else {
			Log.info("There are no live sessions available to verify....");
		}
		
		}
		
	
	      ///////////////// ////////////////   ///////
	//////     //////////////     /////////////   ////////
	     /////////////////  //////////   ////////////
	
/*	public static void clickonLiveSessionsModule(WebDriver driver) throws Exception {
		LiveSessionsPage bookAliveSession = new LiveSessionsPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.lnk_LiveSessions)).click();
		// Log.info("Click action perfomed on Live Sessions main module");
        Thread.sleep(2000);
               
	}
	
	public static void selectPaidSession(WebDriver driver) throws Exception {
		LiveSessionsPage bookAliveSession = new LiveSessionsPage(driver);
		WebDriverWait wait = new WebDriverWait(driver, 60);

		Select drpPrice = new Select(bookAliveSession.drp_SortByPrice);
		drpPrice.selectByIndex(2);
		Thread.sleep(3000);
		if(bookAliveSession.msg_HelpMessageWhenThereAreNoWorkoutsAvailable.isDisplayed()==false) {
	        
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.lnk_WorkoutSession)).click();

		JavascriptExecutor js = (JavascriptExecutor) driver; 
		//This will scroll down the page by 1000 pixel vertical 
		js.executeScript("window.scrollBy(0,500)");
		Thread.sleep(2000);

		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.btn_EnrollNowInSessionPreviewPage)).click();
		//Log.info("Click action performed on Enroll now in session preview page..");
		Thread.sleep(1000);
				
		try {
		bookAliveSession.btn_ConflictConfirmation.isDisplayed();
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.btn_ConflictConfirmation)).click();
		//Log.info("Click action performed on okay button on conflict pop-up..");
				
		} catch (NoSuchElementException  e) {
			// TODO: handle exception
			//e.printStackTrace();
		}
		finally
		{
		    driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		}
		wait.until(ExpectedConditions.elementToBeClickable(bookAliveSession.btn_EnrollNowInConfirmationPopUp)).click();
				
		js.executeScript("window.scrollBy(0,10000)");
		//Log.info("Scroll down till the end of the page");
		
		Thread.sleep(3000);
		}
		else {
			//Log.info("There are no live sessions available to verify....");
			System.out.println("There are no live sessions available to verify....");
		  }
	}
	
	public static void bookASessionWithSavedCards(WebDriver driver) throws InterruptedException {
		
	LiveSessionsPage enrollASessionWithSacedCards = new LiveSessionsPage(driver);
	
	WebDriverWait wait = new WebDriverWait(driver, 60);
		
		if(enrollASessionWithSacedCards.tab_SavedCards.isDisplayed()==true) {
		wait.until(ExpectedConditions.elementToBeClickable(enrollASessionWithSacedCards.btn_PayInCheckoutPage)).click();
		//Log.info("Click on Pay now button in Saved cards tab..");
		Thread.sleep(9000);
		Assert.assertTrue(enrollASessionWithSacedCards.paymentSuccessPage.isDisplayed());
		//Log.info("Payment has done successfully and page has verified successfully...");
		
		Thread.sleep(1500);
		} else {
			System.out.println("No saved card displayed...");
			//Log.info("No saved card displayed...");
		 }
	   }}}
*/	
		
	}



