package com.fitbasewebchecklist.qa.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.fitbasewebchecklist.qa.pages.ChatPage;
import com.fitbasewebchecklist.qa.utilities.Log;


public class ChatAction {

	public static WebDriver driver;
	public static void checkChatFunctionality(WebDriver driver) {
		
		ChatPage chatFun=new ChatPage(driver);
		WebDriverWait wait=new WebDriverWait(driver, 80);
		
		wait.until(ExpectedConditions.elementToBeClickable(chatFun.slider_chat)).click();
		Log.info("Click action ");
		
		
	}
}
