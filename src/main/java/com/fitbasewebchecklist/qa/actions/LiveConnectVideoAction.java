package com.fitbasewebchecklist.qa.actions;

import java.io.File;
import java.util.Iterator;
import java.util.Set;

import org.codehaus.plexus.util.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.fitbasewebchecklist.qa.pages.LiveConnectPage;
import com.fitbasewebchecklist.qa.pages.LoginPage1;
import com.fitbasewebchecklist.qa.utilities.Log;
import com.fitbasewebchecklist.qa.utilities.screenshots;



public class LiveConnectVideoAction {

	public static WebDriver driver;

	public static void checkLiveConnectVideo(WebDriver driver) throws Exception {
		LiveConnectPage liveConnect = new LiveConnectPage(driver);
		LoginPage1 login = new LoginPage1(driver);
		WebDriverWait wait = new WebDriverWait(driver, 80);

		wait.until(ExpectedConditions.elementToBeClickable(liveConnect.btn_LiveSessionsNotificationsPopup)).click();
		Log.info("Click action performed on live sessions pop-up to check live sessions...");

		
			if (liveConnect.msg_NoLiveSessionsAvailable.isDisplayed() == false) {
				wait.until(ExpectedConditions.elementToBeClickable(liveConnect.btn_LiveSession)).click();
				Log.info("Click action performed on active live button to check video...");
				Thread.sleep(18000);
				
			
			  Set<String> window = driver.getWindowHandles(); 
			  Iterator<String> it = window.iterator();
			  
			  String parentWindow = it.next(); 
			  String childWindow = it.next();
			 // String childWindow1 = it.next();
			  driver.switchTo().window(childWindow);
			  Log.info("Switching to live video sessions page..."); 
			  Thread.sleep(5000);
			  
			} else {
				Log.info("There are no live sessions available to check...");
			}
			Thread.sleep(8000);
			screenshots.videostreamlive(driver);
			Thread.sleep(4000);
			driver.findElement(By.xpath("//*[@id=\'icons\']")).isEnabled();
			System.out.println(driver.findElement(By.xpath("//*[@id=\'icons\']")).isDisplayed());
			
			//Log.info("Click action performed on Loginbutton and succesfully Login into Application");
			
			
	} /*
		 * catch (Exception e) { } finally {
		 * driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS); } }
		 */
public static void TrainerLogin(WebDriver driver) throws Exception{
		
		LoginPage1 login = new LoginPage1(driver);
		
		login.TrainerUsername.isDisplayed();
		login.TrainerUsername.clear();
		login.TrainerUsername.sendKeys("trainer@mailinator.com");	
		Thread.sleep(2000);
		Log.info("Entered Trainer valid username");
		
		login.TrainerPassword.isDisplayed();
		login.TrainerPassword.clear();
		login.TrainerPassword.sendKeys("password");
		Thread.sleep(2000);
		Log.info("Entered Trainer valid Password");
		
		login.TrainerLoginBtn.isDisplayed();
		login.TrainerLoginBtn.isEnabled();	
		login.TrainerLoginBtn.click();	
		Thread.sleep(5000);
		
		Assert.assertEquals("Overview" ,"Overview" );
		Log.info("Click action performed on Loginbutton and succesfully Login into Application");
		
		Thread.sleep(7000);
	}
}
