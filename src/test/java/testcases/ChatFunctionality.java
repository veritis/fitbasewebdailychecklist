package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasewebchecklist.qa.helpers.Browserhelper;
import com.fitbasewebchecklist.qa.helpers.ChatHelper;
import com.fitbasewebchecklist.qa.utilities.Log;

public class ChatFunctionality {

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		PropertyConfigurator.configure("log4j.properties");		
		Log.startLog("Test is starting");
      driver=Browserhelper.openBrowser();		
	}
	
	@Test()
	public void chatFunctionality() throws Exception {
		
        ChatHelper.chatFunctionality();
		Reporter.log("Chat functionality testcase has executed sucessfully....");
	}
	
	@AfterClass
	public void quitBrowser() {
		Log.endLog("Test is ending");
		driver.quit();
	}


}
