package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasewebchecklist.qa.actions.SignOutAction;
import com.fitbasewebchecklist.qa.actions.SignUpAction;
import com.fitbasewebchecklist.qa.helpers.Browserhelper;
import com.fitbasewebchecklist.qa.testdata.LoginDataProvider;
import com.fitbasewebchecklist.qa.utilities.Log;

public class SignUp {

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		PropertyConfigurator.configure("log4j.properties");		
		Log.startLog("Test is starting");
		driver = Browserhelper.openBrowser();
	}
	
	@Test(dataProvider="UserSignup", dataProviderClass=LoginDataProvider.class)
	public void signUpFunctionality(String fName, String lName, String pwsd) throws Exception{
		
		SignUpAction.fitbaseSignUpflow(driver, fName, lName, pwsd);
		//SignUpAction.skipHealthDetails(driver);
		SignUpAction.fillHealthDetails(driver);
		SignUpAction.verifyDashboard(driver);
		SignOutAction.signout(driver);
		Reporter.log("Sign up testcase has executed successfully...");
		
	}
	
	@AfterClass
	public void quitBrowser() {
		
		Log.endLog("Test is ending");
		driver.quit();
	}
}
