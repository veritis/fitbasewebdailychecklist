package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasewebchecklist.qa.actions.LoginAction;
import com.fitbasewebchecklist.qa.helpers.Browserhelper;
import com.fitbasewebchecklist.qa.testdata.LoginDataProvider;
import com.fitbasewebchecklist.qa.utilities.Log;

public class FacebookProdLogin {
static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		PropertyConfigurator.configure("log4j.properties");		
		
		Log.startLog("Test is starting");
		driver = Browserhelper.openProdBrowser();
	}
	
	@Test(dataProvider="FacebookLogin", dataProviderClass=LoginDataProvider.class)
	public void facebookLoginFunctionality(String facebookUsername, String facebookPassword) throws InterruptedException {
		LoginAction.fitbaseProdFacebookLogin(driver, facebookUsername, facebookPassword);
	//	SignOutAction.signout(driver);  
		Reporter.log("Login with Facebook testcase has been executed successfully...");

	}
	
	@AfterClass
	public void quitBrowser() {
		Log.endLog("Test is ending");
		driver.close();
	}

}
