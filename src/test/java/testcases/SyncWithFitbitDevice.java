package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasewebchecklist.qa.actions.DevicesAction;
import com.fitbasewebchecklist.qa.actions.LoginAction;
import com.fitbasewebchecklist.qa.helpers.Browserhelper;
import com.fitbasewebchecklist.qa.testdata.LoginDataProvider;
import com.fitbasewebchecklist.qa.utilities.Log;


public class SyncWithFitbitDevice {
	
	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		PropertyConfigurator.configure("log4j.properties");		
		Log.startLog("Test is starting");
		driver=Browserhelper.openBrowser();
	}
	
	
	@Test(dataProvider="FitbitLogin", dataProviderClass = LoginDataProvider.class)
	public void syncWithFitbitFunctionality(String uname, String pswd) throws Exception {
		
		LoginAction.fitbaseNormalLogin(driver, uname, pswd);
		DevicesAction.syncWithFitbitDevice(driver);
	//	SignOutAction.signout(driver);
		Reporter.log("Sync with Fitbit device testcase has executed sucessfully....");

	}
	
	@AfterClass
	public void quitBrowser() {
		Log.endLog("Test is ending");
		driver.quit();
	}

}
