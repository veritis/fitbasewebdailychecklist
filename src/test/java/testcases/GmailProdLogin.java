package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasewebchecklist.qa.actions.LoginAction;
import com.fitbasewebchecklist.qa.helpers.Browserhelper;
import com.fitbasewebchecklist.qa.testdata.LoginDataProvider;
import com.fitbasewebchecklist.qa.utilities.Log;

public class GmailProdLogin {
static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception {
		PropertyConfigurator.configure("log4j.properties");		
		Log.startLog("Test is starting");
		driver = Browserhelper.openProdBrowser();
	}
	
	@Test(dataProvider="GoogleLogin", dataProviderClass=LoginDataProvider.class)
	public void googleLoginFunctionality(String gmailUserName, String gmailPassword) throws Exception{
		LoginAction.fitbaseProdGoogleLogin(driver, gmailUserName, gmailPassword);
		//SignOutAction.signout(driver);  
		Reporter.log("Login with Google testcase has been executed successfully...");
	}
	
	@AfterClass
	public void quitBrowser(){
		Log.endLog("Test is ending");
		driver.quit();
	}
}
