package testcases;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fitbasewebchecklist.qa.actions.LiveConnectVideoAction;
import com.fitbasewebchecklist.qa.actions.LoginAction;
import com.fitbasewebchecklist.qa.helpers.Browserhelper;
import com.fitbasewebchecklist.qa.testdata.LoginDataProvider;
import com.fitbasewebchecklist.qa.utilities.Log;
import com.fitbasewebchecklist.qa.utilities.ScreenRecorderUtil;

public class CheckLiveConnectVideo {

	static WebDriver driver;
	
	@BeforeClass
	public void openBrowser() throws Exception  {
		PropertyConfigurator.configure("log4j.properties");		
		Log.startLog("Test is starting");
			driver=Browserhelper.openBrowser1();
			//ScreenRecorderUtil.startRecord("openBrowser");
			}
	
	@Test(dataProvider="UserLogin", dataProviderClass = LoginDataProvider.class)
	public void videoFunctionality(String uname, String pswd) throws Exception  {
		LoginAction.fitbaseNormalLogin(driver, uname, pswd);
		LiveConnectVideoAction.checkLiveConnectVideo(driver);
		//SignOutAction.signout(driver);
		Reporter.log("Live video session testcase has executed sucessfully....");
		//ScreenRecorderUtil.stopRecord();
		//System.out.println("Video Recording taken");
		}
		
	@AfterClass
	public void closeBrowser() {
		driver.quit();
		Log.endLog("Test is ending");
	}
}
