package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import com.fitbasewebchecklist.qa.actions.LiveConnectVideoAction;
import com.fitbasewebchecklist.qa.actions.LoginAction;
import com.fitbasewebchecklist.qa.testdata.LoginDataProvider;

public class webpush {

	@Test(dataProvider="UserLogin", dataProviderClass = LoginDataProvider.class)
	public void videoFunctionality(String uname, String pswd) throws Exception {
	
//	ChromeOptions options = new ChromeOptions();
//	options.addArguments("--disable-notifications");
//	
//	System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");	
//	WebDriver driver = new ChromeDriver(options);
	
	
	
	ChromeOptions options = new ChromeOptions(); 
	options.addArguments("use-fake-ui-for-media-stream");

	WebDriver driver = new ChromeDriver(options);
	
	
	driver.get("https://qa.fitbase.com/");
	driver.manage().timeouts().pageLoadTimeout(3000, TimeUnit.SECONDS);
	driver.manage().window().maximize();
	
	LoginAction.fitbaseNormalLogin(driver, uname, pswd);
	LiveConnectVideoAction.checkLiveConnectVideo(driver);
	
}
}